// Copyright 2013 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package slp

import "encoding/binary"
import "io"
import "os"

//Used by getLength() to determine the length of the command
const (
	orNext = iota
	andNext
	forceNext
	noNext
)

//32 byte frame header
type FrameHeader struct {
	DataOffset       uint32 //Currently unused
	OutlineOffset    uint32 //Currently unused
	PaletteOffset    uint32 //Currently unused
	PropertiesOffset uint32 //Currently unused
	Width            int32  //Width of the frame
	Height           int32  //Height of the frame
	AnchorX          int32  //X center of the unit
	AnchorY          int32  //Y center of the unit
}

type Frame struct {
	Indices [][]int16 //Color indices. Matching color can be found in the palette. Negative values are possible. See Special Indices
	Edges   []RowEdge //Offsets
	pointer []int     //Row pointers
}

type RowEdge struct {
	Left  uint16 //Offset from the left. 0x8000 means the row is empty
	Right uint16 //Offset from the right. 0x8000 means the row is empty
}

func readFrameHeader(reader io.Reader) (header FrameHeader) {
	binary.Read(reader, binary.LittleEndian, &header)
	return
}

func readFrame(reader io.ReadSeeker, header FrameHeader, player byte) (frame Frame) {
	frame.init(int(header.Height), int(header.Width))
	buffer := make([]byte, 1024*10)

	frame.Edges = make([]RowEdge, header.Height)
	for i := 0; i < int(header.Height); i++ {
		binary.Read(reader, binary.LittleEndian, &frame.Edges[i].Left)
		binary.Read(reader, binary.LittleEndian, &frame.Edges[i].Right)
	}

	CommandOffset := make([]int32, header.Height)
	for i := 0; i < int(header.Height); i++ {
		binary.Read(reader, binary.LittleEndian, &CommandOffset[i])
	}

	//read frames
	for i := 0; i < int(header.Height); i++ {

		//check command offset
		offset, _ := reader.Seek(0, os.SEEK_CUR)
		if CommandOffset[i] != int32(offset) {
			panic("Command offset mismatch!")
		}

		eor := false

		//read command stream
		for !eor {
			var commandByte byte
			binary.Read(reader, binary.LittleEndian, &commandByte)

			command := commandByte & 0x0F // command nibble

			switch command {
			case 0x00, 0x04, 0x08, 0x0C:
				//color list
				length := getLength(reader, commandByte, 2, 1, noNext)
				reader.Read(buffer[:length])
				frame.putPixelArray(i, buffer[:length])

			case 0x01, 0x05, 0x09, 0x0D:
				//skip
				length := getLength(reader, commandByte, 2, 1, orNext)
				frame.skipPixel(i, int(length))

			case 0x02:
				//big color list
				length := getLength(reader, commandByte, 4, 256, andNext)
				reader.Read(buffer[:length])
				frame.putPixelArray(i, buffer[:length])

			case 0x03:
				//big skip
				length := getLength(reader, commandByte, 4, 256, andNext)
				frame.skipPixel(i, int(length))

			case 0x06:
				//player color list
				length := getLength(reader, commandByte, 4, 1, orNext)
				reader.Read(buffer[:length])
				frame.putPlayerPixelArray(i, player, buffer[:length])

			case 0x07:
				//fill
				length := getLength(reader, commandByte, 4, 1, orNext)
				var index byte
				binary.Read(reader, binary.LittleEndian, &index)
				frame.fillPixel(i, length, index)

			case 0x0A:
				//player color fill
				length := getLength(reader, commandByte, 4, 1, orNext)
				var index byte
				binary.Read(reader, binary.LittleEndian, &index)
				frame.fillPlayerPixel(i, length, index, player)

			case 0x0B:
				//shadow transparent
				length := getLength(reader, commandByte, 4, 1, orNext)
				frame.putShadowPixel(i, length)

			case 0x0E:
				//shadow player TODO: implement selectionMask2
				if commandByte == 0x4E || commandByte == 0x6E {
					frame.putSelectionMask(i, 1)
				} else if commandByte == 0x5E || commandByte == 0x7E {
					length := getLength(reader, 0, 0, 0, forceNext)
					frame.putSelectionMask(i, length)
				}

			case 0x0F:
				//end of row
				eor = true

			default:
				panic("unkown command token")
			}
		}
	}

	return
}

func getLength(reader io.Reader, commandByte byte, shift uint, mul int, next int) int {
	length := (int(commandByte) >> shift) * mul
	if (length == 0 && next == orNext) || next == andNext || next == forceNext {
		var extraLength byte
		binary.Read(reader, binary.LittleEndian, &extraLength)
		length += int(extraLength)
	}
	return length
}

func (this *Frame) init(height, width int) {
	this.pointer = make([]int, height)
	this.Indices = make([][]int16, height)
	for i := 0; i < len(this.Indices); i++ {
		this.Indices[i] = make([]int16, width)

		//mask all pixels
		for j := 0; j < len(this.Indices[i]); j++ {
			this.Indices[i][j] = MaskPixel
		}
	}
}

func (this *Frame) putPixel(row int, pixel byte) {
	this.Indices[row][this.pointer[row]] = int16(pixel)
	this.pointer[row]++
}

func (this *Frame) putPixelArray(row int, pixels []byte) {
	for _, p := range pixels {
		this.Indices[row][this.pointer[row]] = int16(p)
		this.pointer[row]++
	}
}

func (this *Frame) putShadowPixel(row int, length int) {
	for i := 0; i < length; i++ {
		this.Indices[row][this.pointer[row]] = ShadowPixel
		this.pointer[row]++
	}
}

func (this *Frame) putSelectionMask(row int, length int) {
	for i := 0; i < length; i++ {
		this.Indices[row][this.pointer[row]] = Outline1Pixel
		this.pointer[row]++
	}
}

func (this *Frame) putPlayerPixelArray(row int, player byte, pixels []byte) {
	for _, p := range pixels {
		this.Indices[row][this.pointer[row]] = int16(p + (player * 16) + 16)
		this.pointer[row]++
	}
}

func (this *Frame) fillPixel(row int, count int, pixel byte) {
	for i := 0; i < count; i++ {
		this.Indices[row][this.pointer[row]] = int16(pixel)
		this.pointer[row]++
	}
}

func (this *Frame) fillPlayerPixel(row int, count int, player byte, pixel byte) {
	for i := 0; i < count; i++ {
		this.Indices[row][this.pointer[row]] = int16(pixel + (player * 16) + 16)
		this.pointer[row]++
	}
}

func (this *Frame) skipPixel(row int, count int) {
	this.pointer[row] += count
}
