// Copyright 2013 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//This package is meant to read and decode slp images used by Age of Empires 2.
// Further reading and thanks to the author(s) of:
//
// http://www.digitization.org/wiki/index.php/SLP
package slp

import (
	"bytes"
	"encoding/binary"
	"image"
	"image/color"
	"image/png"
	"io"
	"os"
	"strconv"
)

//Special Indices
const (
	ShadowPixel   = -1
	Outline1Pixel = -2
	Outline2Pixel = -3
	MaskPixel     = -4 // transparent pixel
)

type Data struct {
	Version        [4]byte       // 2.0N
	PlayerNumber   int           // player number used to decode this slp
	NumberOfFrames int32         //Number of frames in the slp
	Comment        string        // ArtDesk 1.00 SLP Writer
	FrameHeaders   []FrameHeader //Each frame has a corresponding header
	Frames         []Frame
}

//Decodes an slp data chunk and populates the Data struct.
func Decode(reader io.ReadSeeker, player byte) (data Data) {
	//read version number (should be 2.0N)
	binary.Read(reader, binary.BigEndian, &data.Version)

	//read the number of frames
	binary.Read(reader, binary.LittleEndian, &data.NumberOfFrames)

	//read the comment (null terminated string)
	data.Comment = readString(reader)

	//read frame headers
	for i := 0; i < int(data.NumberOfFrames); i++ {
		data.FrameHeaders = append(data.FrameHeaders, readFrameHeader(reader))
	}

	//read frame data
	for i := 0; i < int(data.NumberOfFrames); i++ {
		data.Frames = append(data.Frames, readFrame(reader, data.FrameHeaders[i], player))
	}

	data.PlayerNumber = int(player)

	return
}

//Decodes an slp file and populates the Data struct.
func DecodeFromFile(filename string, player byte) Data {
	file, _ := os.Open(filename)
	defer file.Close()
	size, _ := file.Seek(0, os.SEEK_END)
	file.Seek(0, os.SEEK_SET)

	buffer := make([]byte, size)
	file.Read(buffer)
	reader := bytes.NewReader(buffer)

	return Decode(reader, player)
}

//Decodes and writes all frames in the slp to outputdir.
func WriteImages(outputDir string, data *Data) {
	for i := 0; i < int(data.NumberOfFrames); i++ {
		img := DecodeRGBAImage(&data.FrameHeaders[i], &data.Frames[i])

		writer, err := os.Create(outputDir + "/" + "img" + strconv.FormatInt(int64(i), 10) + ".png")
		if err != nil {
			panic(err)
		}
		png.Encode(writer, img)
	}
}

//Decodes a frame using the corresponding header into an RGBA image.
func DecodeRGBAImage(header *FrameHeader, frame *Frame) *image.RGBA {
	img := image.NewRGBA(image.Rect(0, 0, int(header.Width), int(header.Height)))

	for x := 0; x < len(frame.Indices); x++ {
		for y := 0; y < len(frame.Indices[x]); y++ {
			index := int(frame.Indices[x][y])
			o := int(frame.Edges[x].Left) //offset from the left

			//empty line?
			if o == 0x8000 {
				o = 0
			}

			pixelColor := indexToColor(index)
			img.SetRGBA(o+y, x, pixelColor)
		}
	}
	return img
}

/* ================
Helper functions
================ */

//Maps index to an RGBA color using the slp palette.
func indexToColor(index int) color.RGBA {
	switch index {
	case ShadowPixel:
		return color.RGBA{120, 120, 120, 100}
	case MaskPixel:
		return color.RGBA{0, 0, 0, 0}
	case Outline1Pixel, Outline2Pixel:
		return color.RGBA{0, 0, 0, 0}
	default:
		return color.RGBA{slpPalette[index*3], slpPalette[index*3+1], slpPalette[index*3+2], 255}
	}
	return color.RGBA{}
}

//Reads a null-terminated string.
func readString(reader io.ReadSeeker) string {
	buffer := make([]byte, 0)
	b := byte(0)
	binary.Read(reader, binary.LittleEndian, &b)
	i := 0
	for b != 0 {
		buffer = append(buffer, b)
		binary.Read(reader, binary.LittleEndian, &b)
		i++
		if i >= 32 {
			panic("Malformed comment") //some modtools do not write an null-terminated string here.
		}
	}
	return string(buffer[:])
}
